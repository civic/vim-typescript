compiler typescript

"setlocal autoindent
"setlocal cindent
"setlocal cinoptions=j1,J1
setlocal smartindent
setlocal indentexpr=

setlocal commentstring=//\ %s
